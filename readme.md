
HttpStatusCats
===============

A fun project that was written to add a little personality to your website.  Install this package from NuGet and have your custom error pages reconfigured to show cats acting out each of the Http Status codes.  This collection of error pages is derived from the photos assembled by [GirlieMac on Flickr][1]

How will this change my website?
--------------------------------

The following changes are applied:

 - The Fritz.HttpStatusCats.dll file is referenced
 - `Web.config` is updated to turn on CustomErrors and redirect to the status cat for the 500 error code.
 - `Web.config` is updated to direct IIS to handle HttpErrors with entries in `<system.webServer><httpErrors>`
 - The `LolCatErrorHandler` is configured to respond at `/error.axd`
 - The `LolCatErrorImageHandler` is configured to respond at `/errorImg.axd`


[1]: http://bit.ly/HttpCats