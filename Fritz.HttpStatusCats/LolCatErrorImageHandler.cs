using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace Fritz.HttpStatusCats
{
    public class LolCatErrorImageHandler : BaseErrorHandler, IHttpHandler
    {

        public static Regex reErrorCode = new Regex("errorImg.axd\\?(\\d+)", RegexOptions.Multiline | RegexOptions.CultureInvariant | RegexOptions.Compiled);

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that
        /// implements the <see cref="T:System.Web.IHttpHandler" /> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext" /> object that provides
        /// references to the intrinsic server objects (for example, Request, Response, Session,
        /// and Server) used to service HTTP requests.</param>
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequest(new HttpContextWrapper(context));
        }

        private void ProcessRequest(HttpContextWrapper context)
        {
              
            var errorCode = IdentifyErrorCode(context);

            if (!base.IsErrorCodeImageAvailable(errorCode))
            {
                base.ReturnDefault(context);
                return;
            }

            var theImageStream = GetType().Assembly.GetManifestResourceStream(string.Format("Fritz.HttpStatusCats.img.{0}.jpg", errorCode));;
            var rdr = new BinaryReader(theImageStream);
            context.Response.ContentType = "image/jpeg";
            context.Response.BinaryWrite(rdr.ReadBytes((int)theImageStream.Length));

        }

        private static int IdentifyErrorCode(HttpContextBase context)
        {
            var requestUri = context.Request.Url;
            var matches = reErrorCode.Matches(requestUri.PathAndQuery);
            if (matches.Count == 0)
                throw new Exception("Unable to identify the error code");

            return Convert.ToInt32(matches[0].Groups[1].Value);
        }

        /// <summary>
        /// Gets a value indicating whether another request can use the <see cref="T:System.Web.IHttpHandler" />
        /// instance.
        /// </summary>
        /// <returns>true if the <see cref="T:System.Web.IHttpHandler" /> instance is reusable;
        /// otherwise, false.</returns>
        /// <value></value>
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

    }

}