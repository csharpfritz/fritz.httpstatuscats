﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Fritz.HttpStatusCats
{
    public class LolCatErrorHandler : BaseErrorHandler, IHttpHandler
    {

        private static readonly Regex reErrorCode = new Regex("error.axd\\?(\\d+)", RegexOptions.CultureInvariant | RegexOptions.Compiled); 

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that
        /// implements the <see cref="T:System.Web.IHttpHandler" /> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext" /> object that provides
        /// references to the intrinsic server objects (for example, Request, Response, Session,
        /// and Server) used to service HTTP requests.</param>
        public void ProcessRequest(HttpContextBase context)
        {

            var errorCode = IdentifyErrorCode(context);

            var stream = GetType().Assembly.GetManifestResourceStream("Fritz.HttpStatusCats.Format.html");
            var rdr = new StreamReader(stream);
            var htmlContent = rdr.ReadToEnd();

            htmlContent = htmlContent.Replace("$(ErrorCode)", errorCode.ToString());
            htmlContent = htmlContent.Replace("$(ErrorImage)", string.Format("/errorImg.axd?{0}", errorCode.ToString()));

            if (errorCode < 500)
            {
                context.Response.StatusCode = errorCode;
            }
            context.Response.ContentType = "text/html";
            context.Response.Write(htmlContent);

        }

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that
        /// implements the <see cref="T:System.Web.IHttpHandler" /> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext" /> object that provides
        /// references to the intrinsic server objects (for example, Request, Response, Session,
        /// and Server) used to service HTTP requests.</param>
        public void ProcessRequest(HttpContext context)
        {
            ProcessRequest(new HttpContextWrapper(context));
        }

  
        private static int IdentifyErrorCode(HttpContextBase context)
        {
            var requestUri = context.Request.Url;
            var matches = reErrorCode.Matches(requestUri.PathAndQuery);
            if (matches.Count == 0)
                throw new Exception("Unable to identify the error code");

            return Convert.ToInt32(matches[0].Groups[1].Value);
        }

        /// <summary>
        /// Gets a value indicating whether another request can use the <see cref="T:System.Web.IHttpHandler" />
        /// instance.
        /// </summary>
        /// <returns>true if the <see cref="T:System.Web.IHttpHandler" /> instance is reusable;
        /// otherwise, false.</returns>
        /// <value></value>
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

    }

}
