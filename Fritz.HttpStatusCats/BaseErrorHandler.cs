using System;
using System.Linq;
using System.Web;

namespace Fritz.HttpStatusCats
{
    public abstract class BaseErrorHandler
    {
          
        /// <summary>
        /// Returns the default.
        /// </summary>
        public void ReturnDefault(HttpContextBase context)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        protected bool IsErrorCodeImageAvailable(int code)
        {
              
            string[] names = GetType().Assembly.GetManifestResourceNames();
            return names.Contains(string.Format("Fritz.HttpStatusCats.img.{0}.jpg", code));

        }

    }

}