﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Fritz.HttpStatusCats;
using NUnit.Framework;
using Telerik.JustMock;

namespace Fritz.Test.HttpStatusCats
{

    [TestFixture]
    public class IdentifyErrorCode
    {


        private HttpContextBase _MockHttpContext;

        private HttpRequestBase _MockRequest;

        private HttpResponseBase _MockResponse;

        [SetUp]
        public void TestSetup()
        {
            this._MockHttpContext = Mock.Create<HttpContextBase>();
            this._MockRequest = Mock.Create<HttpRequestBase>();
            this._MockResponse = Mock.Create<HttpResponseBase>();

            Mock.Arrange(_MockHttpContext, c => c.Request).Returns(_MockRequest);
            Mock.Arrange(_MockHttpContext, c => c.Response).Returns(_MockResponse);
        }

        [Test]
        public void ShouldSetHttpStatusCodeApprpropriately()
        {
            
            // Arrange
            var statusCode = 0;
            Mock.Arrange(_MockRequest, r => r.Url).Returns(new Uri("http://localhost/error.axd?400"));
            Mock.ArrangeSet(() => _MockResponse.StatusCode = 400).MustBeCalled();

            // Act
            var sut = new LolCatErrorHandler();
            sut.ProcessRequest(_MockHttpContext);

            // Assert
            Mock.Assert(_MockResponse);

        }

        [Test]
        public void WhenPassedNumericCodeShouldReturnItInHtml()
        {
            
            // Arrange
            string outResponse = "";
            Regex regex = new Regex("Error: (\\d+)", RegexOptions.Multiline | RegexOptions.CultureInvariant | RegexOptions.Compiled);

            Mock.Arrange(_MockRequest, r => r.Url).Returns(new Uri("http://localhost/error.axd?404"));
            Mock.Arrange(() => _MockResponse.Write(Arg.AnyString)).DoInstead<string>((string w) => outResponse += w);

            // Act
            var sut = new LolCatErrorHandler();
            sut.ProcessRequest(_MockHttpContext);

            // Assert
            Console.Out.WriteLine(outResponse);
            var errorCode = regex.Matches(outResponse)[0].Groups[1].Value;
            Assert.AreEqual("404", errorCode, "Did not transmit the correct error code");

        }

    }
}
